#!/bin/bash

set -ex

if [ "$BOARD" = "robin" ]
then
  echo "board: robin"
  export CROSS_COMPILE=arm-linux-gnueabihf-
  export ARCH=arm
  make ARCH=arm xilinx_zynq_defconfig
  make -j 8 UIMAGE_LOADADDR=0x8000 uImage
  mkdir -p iv_output
  cp -f arch/arm/boot/uImage iv_output/uImage
elif [ "$BOARD" = "falconi" ]
then
  echo "board: falconi"
  export CROSS_COMPILE=aarch64-linux-gnu-
  export ARCH=arm64
  make ARCH=arm64 xilinx_zynqmp_defconfig
  make -j 8
  mkdir -p iv_output
  cp arch/arm64/boot/Image.gz iv_output/Image.gz

  # build deepway & inceptio:  phy master mode
  sed -i "s/#define PHY_MASTER_SLAVE_CFG  MASTER_SLAVE_CFG_SLAVE_FORCE/#define PHY_MASTER_SLAVE_CFG  MASTER_SLAVE_CFG_MASTER_FORCE/"  include/linux/phy/phy_customer.h
  make -j 8
  cp arch/arm64/boot/Image.gz iv_output/Image.deepway.gz
  cp arch/arm64/boot/Image.gz iv_output/Image.inceptio.gz

  # restore phy_customer.h
  git checkout -- include/linux/phy/phy_customer.h
else
  echo "BOARD is not valid: ${BOARD}"
  exit 1
fi
