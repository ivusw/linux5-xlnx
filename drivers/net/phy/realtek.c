// SPDX-License-Identifier: GPL-2.0+
/* drivers/net/phy/realtek.c
 *
 * Driver for Realtek PHYs
 *
 * Author: Johnson Leung <r58129@freescale.com>
 *
 * Copyright (c) 2004 Freescale Semiconductor, Inc.
 */
#include <linux/bitops.h>
#include <linux/of.h>
#include <linux/phy.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/phy/phy_customer.h>

#define RTL821x_PHYSR				0x11
#define RTL821x_PHYSR_DUPLEX			BIT(13)
#define RTL821x_PHYSR_SPEED			GENMASK(15, 14)

#define RTL821x_INER				0x12
#define RTL8211B_INER_INIT			0x6400
#define RTL8211E_INER_LINK_STATUS		BIT(10)
#define RTL8211F_INER_LINK_STATUS		BIT(4)

#define RTL821x_INSR				0x13

#define RTL821x_EXT_PAGE_SELECT			0x1e
#define RTL821x_PAGE_SELECT			0x1f

#define RTL8211F_PHYCR1				0x18
#define RTL8211F_PHYCR2				0x19
#define RTL8211F_INSR				0x1d

#define RTL8211F_TX_DELAY			BIT(8)
#define RTL8211F_RX_DELAY			BIT(3)

#define RTL8211F_ALDPS_PLL_OFF			BIT(1)
#define RTL8211F_ALDPS_ENABLE			BIT(2)
#define RTL8211F_ALDPS_XTAL_OFF			BIT(12)

#define RTL8211E_CTRL_DELAY			BIT(13)
#define RTL8211E_TX_DELAY			BIT(12)
#define RTL8211E_RX_DELAY			BIT(11)

#define RTL8211F_CLKOUT_EN			BIT(0)

#define RTL8201F_ISR				0x1e
#define RTL8201F_ISR_ANERR			BIT(15)
#define RTL8201F_ISR_DUPLEX			BIT(13)
#define RTL8201F_ISR_LINK			BIT(11)
#define RTL8201F_ISR_MASK			(RTL8201F_ISR_ANERR | \
						 RTL8201F_ISR_DUPLEX | \
						 RTL8201F_ISR_LINK)
#define RTL8201F_IER				0x13

#define RTL8366RB_POWER_SAVE			0x15
#define RTL8366RB_POWER_SAVE_ON			BIT(12)

#define RTL_SUPPORTS_5000FULL			BIT(14)
#define RTL_SUPPORTS_2500FULL			BIT(13)
#define RTL_SUPPORTS_10000FULL			BIT(0)
#define RTL_ADV_2500FULL			BIT(7)
#define RTL_LPADV_10000FULL			BIT(11)
#define RTL_LPADV_5000FULL			BIT(6)
#define RTL_LPADV_2500FULL			BIT(5)

#define RTL9000A_GINMR				0x14
#define RTL9000A_GINMR_LINK_STATUS		BIT(4)

#define RTL9010AX_SPR_ADDR			0x1b
#define RTL9010AX_SPR_DATA			0x1c

#define RTLGEN_SPEED_MASK			0x0630

#define RTL_GENERIC_PHYID			0x001cc800


 // Refer to Errata_List_v0.4.pdf, Erratum#3
 typedef enum {
	Weak_RGMII_3V3    = 1,
	Typical_RGMII_3V3 = 2,
	Strong_RGMII_3V3  = 3,
	Weak_RGMII_2V5    = 4,
	Typical_RGMII_2V5 = 5,
	Strong_RGMII_2V5  = 6,
	Weak_RGMII_1V8    = 7,
	Typical_RGMII_1V8 = 8,
	Strong_RGMII_1V8  = 9
} RGMII_Voltage;

MODULE_DESCRIPTION("Realtek PHY driver");
MODULE_AUTHOR("Johnson Leung");
MODULE_LICENSE("GPL");

struct rtl821x_priv {
	u16 phycr1;
	u16 phycr2;
};

struct rtl9010ax_priv {
	int master_slave;
};

static int rtl821x_read_page(struct phy_device *phydev)
{
	return __phy_read(phydev, RTL821x_PAGE_SELECT);
}

static int rtl821x_write_page(struct phy_device *phydev, int page)
{
	return __phy_write(phydev, RTL821x_PAGE_SELECT, page);
}

static int rtl821x_probe(struct phy_device *phydev)
{
	struct device *dev = &phydev->mdio.dev;
	struct rtl821x_priv *priv;
	int ret;

	priv = devm_kzalloc(dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	ret = phy_read_paged(phydev, 0xa43, RTL8211F_PHYCR1);
	if (ret < 0)
		return ret;

	priv->phycr1 = ret & (RTL8211F_ALDPS_PLL_OFF | RTL8211F_ALDPS_ENABLE | RTL8211F_ALDPS_XTAL_OFF);
	if (of_property_read_bool(dev->of_node, "realtek,aldps-enable"))
		priv->phycr1 |= RTL8211F_ALDPS_PLL_OFF | RTL8211F_ALDPS_ENABLE | RTL8211F_ALDPS_XTAL_OFF;

	ret = phy_read_paged(phydev, 0xa43, RTL8211F_PHYCR2);
	if (ret < 0)
		return ret;

	priv->phycr2 = ret & RTL8211F_CLKOUT_EN;
	if (of_property_read_bool(dev->of_node, "realtek,clkout-disable"))
		priv->phycr2 &= ~RTL8211F_CLKOUT_EN;

	phydev->priv = priv;

	return 0;
}

static int rtl8201_ack_interrupt(struct phy_device *phydev)
{
	int err;

	err = phy_read(phydev, RTL8201F_ISR);

	return (err < 0) ? err : 0;
}

static int rtl821x_ack_interrupt(struct phy_device *phydev)
{
	int err;

	err = phy_read(phydev, RTL821x_INSR);

	return (err < 0) ? err : 0;
}

static int rtl8211f_ack_interrupt(struct phy_device *phydev)
{
	int err;

	err = phy_read_paged(phydev, 0xa43, RTL8211F_INSR);

	return (err < 0) ? err : 0;
}

static int rtl8201_config_intr(struct phy_device *phydev)
{
	u16 val;
	int err;

	if (phydev->interrupts == PHY_INTERRUPT_ENABLED) {
		err = rtl8201_ack_interrupt(phydev);
		if (err)
			return err;

		val = BIT(13) | BIT(12) | BIT(11);
		err = phy_write_paged(phydev, 0x7, RTL8201F_IER, val);
	} else {
		val = 0;
		err = phy_write_paged(phydev, 0x7, RTL8201F_IER, val);
		if (err)
			return err;

		err = rtl8201_ack_interrupt(phydev);
	}

	return err;
}

static int rtl8211b_config_intr(struct phy_device *phydev)
{
	int err;

	if (phydev->interrupts == PHY_INTERRUPT_ENABLED) {
		err = rtl821x_ack_interrupt(phydev);
		if (err)
			return err;

		err = phy_write(phydev, RTL821x_INER,
				RTL8211B_INER_INIT);
	} else {
		err = phy_write(phydev, RTL821x_INER, 0);
		if (err)
			return err;

		err = rtl821x_ack_interrupt(phydev);
	}

	return err;
}

static int rtl8211e_config_intr(struct phy_device *phydev)
{
	int err;

	if (phydev->interrupts == PHY_INTERRUPT_ENABLED) {
		err = rtl821x_ack_interrupt(phydev);
		if (err)
			return err;

		err = phy_write(phydev, RTL821x_INER,
				RTL8211E_INER_LINK_STATUS);
	} else {
		err = phy_write(phydev, RTL821x_INER, 0);
		if (err)
			return err;

		err = rtl821x_ack_interrupt(phydev);
	}

	return err;
}

static int rtl8211f_config_intr(struct phy_device *phydev)
{
	u16 val;
	int err;

	if (phydev->interrupts == PHY_INTERRUPT_ENABLED) {
		err = rtl8211f_ack_interrupt(phydev);
		if (err)
			return err;

		val = RTL8211F_INER_LINK_STATUS;
		err = phy_write_paged(phydev, 0xa42, RTL821x_INER, val);
	} else {
		val = 0;
		err = phy_write_paged(phydev, 0xa42, RTL821x_INER, val);
		if (err)
			return err;

		err = rtl8211f_ack_interrupt(phydev);
	}

	return err;
}

static irqreturn_t rtl8201_handle_interrupt(struct phy_device *phydev)
{
	int irq_status;

	irq_status = phy_read(phydev, RTL8201F_ISR);
	if (irq_status < 0) {
		phy_error(phydev);
		return IRQ_NONE;
	}

	if (!(irq_status & RTL8201F_ISR_MASK))
		return IRQ_NONE;

	phy_trigger_machine(phydev);

	return IRQ_HANDLED;
}

static irqreturn_t rtl821x_handle_interrupt(struct phy_device *phydev)
{
	int irq_status, irq_enabled;

	irq_status = phy_read(phydev, RTL821x_INSR);
	if (irq_status < 0) {
		phy_error(phydev);
		return IRQ_NONE;
	}

	irq_enabled = phy_read(phydev, RTL821x_INER);
	if (irq_enabled < 0) {
		phy_error(phydev);
		return IRQ_NONE;
	}

	if (!(irq_status & irq_enabled))
		return IRQ_NONE;

	phy_trigger_machine(phydev);

	return IRQ_HANDLED;
}

static irqreturn_t rtl8211f_handle_interrupt(struct phy_device *phydev)
{
	int irq_status;

	irq_status = phy_read_paged(phydev, 0xa43, RTL8211F_INSR);
	if (irq_status < 0) {
		phy_error(phydev);
		return IRQ_NONE;
	}

	if (!(irq_status & RTL8211F_INER_LINK_STATUS))
		return IRQ_NONE;

	phy_trigger_machine(phydev);

	return IRQ_HANDLED;
}

static int rtl8211_config_aneg(struct phy_device *phydev)
{
	int ret;

	ret = genphy_config_aneg(phydev);
	if (ret < 0)
		return ret;

	/* Quirk was copied from vendor driver. Unfortunately it includes no
	 * description of the magic numbers.
	 */
	if (phydev->speed == SPEED_100 && phydev->autoneg == AUTONEG_DISABLE) {
		phy_write(phydev, 0x17, 0x2138);
		phy_write(phydev, 0x0e, 0x0260);
	} else {
		phy_write(phydev, 0x17, 0x2108);
		phy_write(phydev, 0x0e, 0x0000);
	}

	return 0;
}

static int rtl8211c_config_init(struct phy_device *phydev)
{
	/* RTL8211C has an issue when operating in Gigabit slave mode */
	return phy_set_bits(phydev, MII_CTRL1000,
			    CTL1000_ENABLE_MASTER | CTL1000_AS_MASTER);
}

static int rtl8211f_config_init(struct phy_device *phydev)
{
	struct rtl821x_priv *priv = phydev->priv;
	struct device *dev = &phydev->mdio.dev;
	u16 val_txdly, val_rxdly;
	int ret;

	ret = phy_modify_paged_changed(phydev, 0xa43, RTL8211F_PHYCR1,
				       RTL8211F_ALDPS_PLL_OFF | RTL8211F_ALDPS_ENABLE | RTL8211F_ALDPS_XTAL_OFF,
				       priv->phycr1);
	if (ret < 0) {
		dev_err(dev, "aldps mode  configuration failed: %pe\n",
			ERR_PTR(ret));
		return ret;
	}

	switch (phydev->interface) {
	case PHY_INTERFACE_MODE_RGMII:
		val_txdly = 0;
		val_rxdly = 0;
		break;

	case PHY_INTERFACE_MODE_RGMII_RXID:
		val_txdly = 0;
		val_rxdly = RTL8211F_RX_DELAY;
		break;

	case PHY_INTERFACE_MODE_RGMII_TXID:
		val_txdly = RTL8211F_TX_DELAY;
		val_rxdly = 0;
		break;

	case PHY_INTERFACE_MODE_RGMII_ID:
		val_txdly = RTL8211F_TX_DELAY;
		val_rxdly = RTL8211F_RX_DELAY;
		break;

	default: /* the rest of the modes imply leaving delay as is. */
		return 0;
	}

	ret = phy_modify_paged_changed(phydev, 0xd08, 0x11, RTL8211F_TX_DELAY,
				       val_txdly);
	if (ret < 0) {
		dev_err(dev, "Failed to update the TX delay register\n");
		return ret;
	} else if (ret) {
		dev_dbg(dev,
			"%s 2ns TX delay (and changing the value from pin-strapping RXD1 or the bootloader)\n",
			val_txdly ? "Enabling" : "Disabling");
	} else {
		dev_dbg(dev,
			"2ns TX delay was already %s (by pin-strapping RXD1 or bootloader configuration)\n",
			val_txdly ? "enabled" : "disabled");
	}

	ret = phy_modify_paged_changed(phydev, 0xd08, 0x15, RTL8211F_RX_DELAY,
				       val_rxdly);
	if (ret < 0) {
		dev_err(dev, "Failed to update the RX delay register\n");
		return ret;
	} else if (ret) {
		dev_dbg(dev,
			"%s 2ns RX delay (and changing the value from pin-strapping RXD0 or the bootloader)\n",
			val_rxdly ? "Enabling" : "Disabling");
	} else {
		dev_dbg(dev,
			"2ns RX delay was already %s (by pin-strapping RXD0 or bootloader configuration)\n",
			val_rxdly ? "enabled" : "disabled");
	}

	ret = phy_modify_paged(phydev, 0xa43, RTL8211F_PHYCR2,
			       RTL8211F_CLKOUT_EN, priv->phycr2);
	if (ret < 0) {
		dev_err(dev, "clkout configuration failed: %pe\n",
			ERR_PTR(ret));
		return ret;
	}

	return genphy_soft_reset(phydev);
}

static int rtl821x_resume(struct phy_device *phydev)
{
	int ret;

	ret = genphy_resume(phydev);
	if (ret < 0)
		return ret;

	msleep(20);

	return 0;
}

static int rtl8211e_config_init(struct phy_device *phydev)
{
	int ret = 0, oldpage;
	u16 val;

	/* enable TX/RX delay for rgmii-* modes, and disable them for rgmii. */
	switch (phydev->interface) {
	case PHY_INTERFACE_MODE_RGMII:
		val = RTL8211E_CTRL_DELAY | 0;
		break;
	case PHY_INTERFACE_MODE_RGMII_ID:
		val = RTL8211E_CTRL_DELAY | RTL8211E_TX_DELAY | RTL8211E_RX_DELAY;
		break;
	case PHY_INTERFACE_MODE_RGMII_RXID:
		val = RTL8211E_CTRL_DELAY | RTL8211E_RX_DELAY;
		break;
	case PHY_INTERFACE_MODE_RGMII_TXID:
		val = RTL8211E_CTRL_DELAY | RTL8211E_TX_DELAY;
		break;
	default: /* the rest of the modes imply leaving delays as is. */
		return 0;
	}

	/* According to a sample driver there is a 0x1c config register on the
	 * 0xa4 extension page (0x7) layout. It can be used to disable/enable
	 * the RX/TX delays otherwise controlled by RXDLY/TXDLY pins.
	 * The configuration register definition:
	 * 14 = reserved
	 * 13 = Force Tx RX Delay controlled by bit12 bit11,
	 * 12 = RX Delay, 11 = TX Delay
	 * 10:0 = Test && debug settings reserved by realtek
	 */
	oldpage = phy_select_page(phydev, 0x7);
	if (oldpage < 0)
		goto err_restore_page;

	ret = __phy_write(phydev, RTL821x_EXT_PAGE_SELECT, 0xa4);
	if (ret)
		goto err_restore_page;

	ret = __phy_modify(phydev, 0x1c, RTL8211E_CTRL_DELAY
			   | RTL8211E_TX_DELAY | RTL8211E_RX_DELAY,
			   val);

err_restore_page:
	return phy_restore_page(phydev, oldpage, ret);
}

static int rtl8211b_suspend(struct phy_device *phydev)
{
	phy_write(phydev, MII_MMD_DATA, BIT(9));

	return genphy_suspend(phydev);
}

static int rtl8211b_resume(struct phy_device *phydev)
{
	phy_write(phydev, MII_MMD_DATA, 0);

	return genphy_resume(phydev);
}

static int rtl8366rb_config_init(struct phy_device *phydev)
{
	int ret;

	ret = phy_set_bits(phydev, RTL8366RB_POWER_SAVE,
			   RTL8366RB_POWER_SAVE_ON);
	if (ret) {
		dev_err(&phydev->mdio.dev,
			"error enabling power management\n");
	}

	return ret;
}

/* get actual speed to cover the downshift case */
static int rtlgen_get_speed(struct phy_device *phydev)
{
	int val;

	if (!phydev->link)
		return 0;

	val = phy_read_paged(phydev, 0xa43, 0x12);
	if (val < 0)
		return val;

	switch (val & RTLGEN_SPEED_MASK) {
	case 0x0000:
		phydev->speed = SPEED_10;
		break;
	case 0x0010:
		phydev->speed = SPEED_100;
		break;
	case 0x0020:
		phydev->speed = SPEED_1000;
		break;
	case 0x0200:
		phydev->speed = SPEED_10000;
		break;
	case 0x0210:
		phydev->speed = SPEED_2500;
		break;
	case 0x0220:
		phydev->speed = SPEED_5000;
		break;
	default:
		break;
	}

	return 0;
}

static int rtlgen_read_status(struct phy_device *phydev)
{
	int ret;

	ret = genphy_read_status(phydev);
	if (ret < 0)
		return ret;

	return rtlgen_get_speed(phydev);
}

static int rtlgen_read_mmd(struct phy_device *phydev, int devnum, u16 regnum)
{
	int ret;

	if (devnum == MDIO_MMD_PCS && regnum == MDIO_PCS_EEE_ABLE) {
		rtl821x_write_page(phydev, 0xa5c);
		ret = __phy_read(phydev, 0x12);
		rtl821x_write_page(phydev, 0);
	} else if (devnum == MDIO_MMD_AN && regnum == MDIO_AN_EEE_ADV) {
		rtl821x_write_page(phydev, 0xa5d);
		ret = __phy_read(phydev, 0x10);
		rtl821x_write_page(phydev, 0);
	} else if (devnum == MDIO_MMD_AN && regnum == MDIO_AN_EEE_LPABLE) {
		rtl821x_write_page(phydev, 0xa5d);
		ret = __phy_read(phydev, 0x11);
		rtl821x_write_page(phydev, 0);
	} else {
		ret = -EOPNOTSUPP;
	}

	return ret;
}

static int rtlgen_write_mmd(struct phy_device *phydev, int devnum, u16 regnum,
			    u16 val)
{
	int ret;

	if (devnum == MDIO_MMD_AN && regnum == MDIO_AN_EEE_ADV) {
		rtl821x_write_page(phydev, 0xa5d);
		ret = __phy_write(phydev, 0x10, val);
		rtl821x_write_page(phydev, 0);
	} else {
		ret = -EOPNOTSUPP;
	}

	return ret;
}

static int rtl822x_read_mmd(struct phy_device *phydev, int devnum, u16 regnum)
{
	int ret = rtlgen_read_mmd(phydev, devnum, regnum);

	if (ret != -EOPNOTSUPP)
		return ret;

	if (devnum == MDIO_MMD_PCS && regnum == MDIO_PCS_EEE_ABLE2) {
		rtl821x_write_page(phydev, 0xa6e);
		ret = __phy_read(phydev, 0x16);
		rtl821x_write_page(phydev, 0);
	} else if (devnum == MDIO_MMD_AN && regnum == MDIO_AN_EEE_ADV2) {
		rtl821x_write_page(phydev, 0xa6d);
		ret = __phy_read(phydev, 0x12);
		rtl821x_write_page(phydev, 0);
	} else if (devnum == MDIO_MMD_AN && regnum == MDIO_AN_EEE_LPABLE2) {
		rtl821x_write_page(phydev, 0xa6d);
		ret = __phy_read(phydev, 0x10);
		rtl821x_write_page(phydev, 0);
	}

	return ret;
}

static int rtl822x_write_mmd(struct phy_device *phydev, int devnum, u16 regnum,
			     u16 val)
{
	int ret = rtlgen_write_mmd(phydev, devnum, regnum, val);

	if (ret != -EOPNOTSUPP)
		return ret;

	if (devnum == MDIO_MMD_AN && regnum == MDIO_AN_EEE_ADV2) {
		rtl821x_write_page(phydev, 0xa6d);
		ret = __phy_write(phydev, 0x12, val);
		rtl821x_write_page(phydev, 0);
	}

	return ret;
}

static int rtl822x_get_features(struct phy_device *phydev)
{
	int val;

	val = phy_read_paged(phydev, 0xa61, 0x13);
	if (val < 0)
		return val;

	linkmode_mod_bit(ETHTOOL_LINK_MODE_2500baseT_Full_BIT,
			 phydev->supported, val & RTL_SUPPORTS_2500FULL);
	linkmode_mod_bit(ETHTOOL_LINK_MODE_5000baseT_Full_BIT,
			 phydev->supported, val & RTL_SUPPORTS_5000FULL);
	linkmode_mod_bit(ETHTOOL_LINK_MODE_10000baseT_Full_BIT,
			 phydev->supported, val & RTL_SUPPORTS_10000FULL);

	return genphy_read_abilities(phydev);
}

static int rtl822x_config_aneg(struct phy_device *phydev)
{
	int ret = 0;

	if (phydev->autoneg == AUTONEG_ENABLE) {
		u16 adv2500 = 0;

		if (linkmode_test_bit(ETHTOOL_LINK_MODE_2500baseT_Full_BIT,
				      phydev->advertising))
			adv2500 = RTL_ADV_2500FULL;

		ret = phy_modify_paged_changed(phydev, 0xa5d, 0x12,
					       RTL_ADV_2500FULL, adv2500);
		if (ret < 0)
			return ret;
	}

	return __genphy_config_aneg(phydev, ret);
}

static int rtl822x_read_status(struct phy_device *phydev)
{
	int ret;

	if (phydev->autoneg == AUTONEG_ENABLE) {
		int lpadv = phy_read_paged(phydev, 0xa5d, 0x13);

		if (lpadv < 0)
			return lpadv;

		linkmode_mod_bit(ETHTOOL_LINK_MODE_10000baseT_Full_BIT,
			phydev->lp_advertising, lpadv & RTL_LPADV_10000FULL);
		linkmode_mod_bit(ETHTOOL_LINK_MODE_5000baseT_Full_BIT,
			phydev->lp_advertising, lpadv & RTL_LPADV_5000FULL);
		linkmode_mod_bit(ETHTOOL_LINK_MODE_2500baseT_Full_BIT,
			phydev->lp_advertising, lpadv & RTL_LPADV_2500FULL);
	}

	ret = genphy_read_status(phydev);
	if (ret < 0)
		return ret;

	return rtlgen_get_speed(phydev);
}

static bool rtlgen_supports_2_5gbps(struct phy_device *phydev)
{
	int val;

	phy_write(phydev, RTL821x_PAGE_SELECT, 0xa61);
	val = phy_read(phydev, 0x13);
	phy_write(phydev, RTL821x_PAGE_SELECT, 0);

	return val >= 0 && val & RTL_SUPPORTS_2500FULL;
}

static int rtlgen_match_phy_device(struct phy_device *phydev)
{
	return phydev->phy_id == RTL_GENERIC_PHYID &&
	       !rtlgen_supports_2_5gbps(phydev);
}

static int rtl8226_match_phy_device(struct phy_device *phydev)
{
	return phydev->phy_id == RTL_GENERIC_PHYID &&
	       rtlgen_supports_2_5gbps(phydev);
}

static int rtlgen_resume(struct phy_device *phydev)
{
	int ret = genphy_resume(phydev);

	/* Internal PHY's from RTL8168h up may not be instantly ready */
	msleep(20);

	return ret;
}

static int rtl9000a_config_init(struct phy_device *phydev)
{
	phydev->autoneg = AUTONEG_DISABLE;
	phydev->speed = SPEED_100;
	phydev->duplex = DUPLEX_FULL;

	return 0;
}

static int rtl9000a_config_aneg(struct phy_device *phydev)
{
	int ret;
	u16 ctl = 0;

	switch (phydev->master_slave_set) {
	case MASTER_SLAVE_CFG_MASTER_FORCE:
		ctl |= CTL1000_AS_MASTER;
		break;
	case MASTER_SLAVE_CFG_SLAVE_FORCE:
		break;
	case MASTER_SLAVE_CFG_UNKNOWN:
	case MASTER_SLAVE_CFG_UNSUPPORTED:
		return 0;
	default:
		phydev_warn(phydev, "Unsupported Master/Slave mode\n");
		return -EOPNOTSUPP;
	}

	ret = phy_modify_changed(phydev, MII_CTRL1000, CTL1000_AS_MASTER, ctl);
	if (ret == 1)
		ret = genphy_soft_reset(phydev);

	return ret;
}

static int rtl9000a_read_status(struct phy_device *phydev)
{
	int ret;

	phydev->master_slave_get = MASTER_SLAVE_CFG_UNKNOWN;
	phydev->master_slave_state = MASTER_SLAVE_STATE_UNKNOWN;

	ret = genphy_update_link(phydev);
	if (ret)
		return ret;

	ret = phy_read(phydev, MII_CTRL1000);
	if (ret < 0)
		return ret;
	if (ret & CTL1000_AS_MASTER)
		phydev->master_slave_get = MASTER_SLAVE_CFG_MASTER_FORCE;
	else
		phydev->master_slave_get = MASTER_SLAVE_CFG_SLAVE_FORCE;

	ret = phy_read(phydev, MII_STAT1000);
	if (ret < 0)
		return ret;
	if (ret & LPA_1000MSRES)
		phydev->master_slave_state = MASTER_SLAVE_STATE_MASTER;
	else
		phydev->master_slave_state = MASTER_SLAVE_STATE_SLAVE;

	return 0;
}

static int rtl9000a_ack_interrupt(struct phy_device *phydev)
{
	int err;

	err = phy_read(phydev, RTL8211F_INSR);

	return (err < 0) ? err : 0;
}

static int rtl9000a_config_intr(struct phy_device *phydev)
{
	u16 val;
	int err;

	if (phydev->interrupts == PHY_INTERRUPT_ENABLED) {
		err = rtl9000a_ack_interrupt(phydev);
		if (err)
			return err;

		val = (u16)~RTL9000A_GINMR_LINK_STATUS;
		err = phy_write_paged(phydev, 0xa42, RTL9000A_GINMR, val);
	} else {
		val = ~0;
		err = phy_write_paged(phydev, 0xa42, RTL9000A_GINMR, val);
		if (err)
			return err;

		err = rtl9000a_ack_interrupt(phydev);
	}

	return phy_write_paged(phydev, 0xa42, RTL9000A_GINMR, val);
}

static irqreturn_t rtl9000a_handle_interrupt(struct phy_device *phydev)
{
	int irq_status;

	irq_status = phy_read(phydev, RTL8211F_INSR);
	if (irq_status < 0) {
		phy_error(phydev);
		return IRQ_NONE;
	}

	if (!(irq_status & RTL8211F_INER_LINK_STATUS))
		return IRQ_NONE;

	phy_trigger_machine(phydev);

	return IRQ_HANDLED;
}

static int __rtl9010ax_read_spr(struct phy_device *phydev, u16 spr)
{
	int val;

	val = __phy_write(phydev, RTL9010AX_SPR_ADDR, spr);
	if (val < 0)
		return val;

	return __phy_read(phydev, RTL9010AX_SPR_DATA);
}

static int rtl9010ax_read_spr(struct phy_device *phydev, u16 spr)
{
	int ret;

	phy_lock_mdio_bus(phydev);
	ret = __rtl9010ax_read_spr(phydev, spr);
	phy_unlock_mdio_bus(phydev);

	return ret;
}

static int __rtl9010ax_write_spr(struct phy_device *phydev, u16 spr, u16 val)
{
	int ret;

	ret = __phy_write(phydev, RTL9010AX_SPR_ADDR, spr);
	if (ret < 0)
		return ret;

	return __phy_write(phydev, RTL9010AX_SPR_DATA, val);
}

static int rtl9010ax_write_spr(struct phy_device *phydev, u16 spr, u16 val)
{
	int ret;

	phy_lock_mdio_bus(phydev);
	ret = __rtl9010ax_write_spr(phydev, spr, val);
	phy_unlock_mdio_bus(phydev);

	return ret;
}

static int rtl9010ax_soft_reset(struct phy_device *phydev)
{
	u16 res = BMCR_RESET;
	int ret, val;

	if (phydev->autoneg == AUTONEG_ENABLE)
		res |= BMCR_ANRESTART;

	ret = phy_modify(phydev, MII_BMCR, BMCR_ISOLATE, res);
	if (ret < 0)
		return ret;

	/* Clause 22 states that setting bit BMCR_RESET sets control registers
	 * to their default value. Therefore the POWER DOWN bit is supposed to
	 * be cleared after soft reset.
	 */
	phydev->suspended = 0;

	ret = phy_read_poll_timeout(phydev, MII_BMCR, val, (val == 0x0140),
					1000, 20000, true);
	if (ret)
		return ret;

	/* BMCR may be reset to defaults */
	if (phydev->autoneg == AUTONEG_DISABLE)
		ret = genphy_setup_forced(phydev);

	return ret;
}

// Refer to Errata_List_v0.4.pdf, Erratum#1
static int rtl9010ax_initial_config(struct phy_device *phydev)
{
	int ret;
	u32 timer;

	// PHY Parameter Start
	phy_write_paged(phydev, 0x0bc4, 0x15, 0x16fe);

	rtl9010ax_write_spr(phydev, 0xb820, 0x0010);
	rtl9010ax_write_spr(phydev, 0xb830, 0x8000);
	timer = 3;
	for(;;) {
		ret = rtl9010ax_read_spr(phydev, 0xb800);
		if (ret < 0)
			return ret;
		if (ret & 0x0040)
			break;
		if (--timer == 0)
			return -ETIMEDOUT;
		mdelay(1);
	}

	rtl9010ax_write_spr(phydev, 0x8020, 0x9100);
	rtl9010ax_write_spr(phydev, 0xb82e, 0x0001);
	rtl9010ax_write_spr(phydev, 0xb820, 0x0290);
	rtl9010ax_write_spr(phydev, 0xa012, 0x0000);
	rtl9010ax_write_spr(phydev, 0xa014, 0xd700);
	phy_write(phydev, 0x1c, 0x880f);
	phy_write(phydev, 0x1c, 0x262d);
	rtl9010ax_write_spr(phydev, 0xa01a, 0x0000);
	rtl9010ax_write_spr(phydev, 0xa000, 0x162c);
	rtl9010ax_write_spr(phydev, 0xb820, 0x0210);
	rtl9010ax_write_spr(phydev, 0xb82e, 0x0000);
	rtl9010ax_write_spr(phydev, 0x8020, 0x0000);
	rtl9010ax_write_spr(phydev, 0xb820, 0x0000);
	timer = 3;
	for (;;) {
		ret = rtl9010ax_read_spr(phydev, 0xb800);
		if (ret < 0)
			return ret;
		if (!(ret & 0x0040))
			break;
		if (--timer == 0)
			return -ETIMEDOUT;
		mdelay(1);
	}

	return rtl9010ax_soft_reset(phydev);
}

// Refer to Errata_List_v0.4.pdf, Erratum#4
static int rtl9010ax_initial_sgmii(struct phy_device *phydev)
{
	int ret;
	u32 timer;

	rtl9010ax_write_spr(phydev, 0xB820, 0x0010);
	rtl9010ax_write_spr(phydev, 0xB830, 0x8000);
	timer = 3;
	for (;;) {
		ret = rtl9010ax_read_spr(phydev, 0xB800);
		if (ret < 0)
			return ret;
		if (ret & 0x0040)
			break;
		if (--timer == 0)
			return -ETIMEDOUT;
		mdelay(1);
	}

	rtl9010ax_write_spr(phydev, 0x8020, 0x9100);
	rtl9010ax_write_spr(phydev, 0xB82E, 0x0001);
	rtl9010ax_write_spr(phydev, 0x8614, 0xAF86);
	phy_write(phydev, 0x1c, 0x20AF);
	phy_write(phydev, 0x1c, 0x862A);
	phy_write(phydev, 0x1c, 0xAF86);
	phy_write(phydev, 0x1c, 0x2AAF);
	phy_write(phydev, 0x1c, 0x862A);
	phy_write(phydev, 0x1c, 0xEE82);
	phy_write(phydev, 0x1c, 0xF600);
	phy_write(phydev, 0x1c, 0x0286);
	phy_write(phydev, 0x1c, 0x2AAF);
	phy_write(phydev, 0x1c, 0x096B);
	phy_write(phydev, 0x1c, 0xF8F9);
	phy_write(phydev, 0x1c, 0xEF59);
	phy_write(phydev, 0x1c, 0xF9FA);
	phy_write(phydev, 0x1c, 0xFBBF);
	phy_write(phydev, 0x1c, 0x8768);
	phy_write(phydev, 0x1c, 0x023C);
	phy_write(phydev, 0x1c, 0x5F00);
	phy_write(phydev, 0x1c, 0x0000);
	phy_write(phydev, 0x1c, 0xAD28);
	phy_write(phydev, 0x1c, 0x1AAE);
	phy_write(phydev, 0x1c, 0x03AF);
	phy_write(phydev, 0x1c, 0x86E4);
	phy_write(phydev, 0x1c, 0xE187);
	phy_write(phydev, 0x1c, 0x6BD0);
	phy_write(phydev, 0x1c, 0x001B);
	phy_write(phydev, 0x1c, 0x019E);
	phy_write(phydev, 0x1c, 0x13D0);
	phy_write(phydev, 0x1c, 0x011B);
	phy_write(phydev, 0x1c, 0x019E);
	phy_write(phydev, 0x1c, 0x34D0);
	phy_write(phydev, 0x1c, 0x021B);
	phy_write(phydev, 0x1c, 0x019E);
	phy_write(phydev, 0x1c, 0xE8EE);
	phy_write(phydev, 0x1c, 0x876B);
	phy_write(phydev, 0x1c, 0x00AF);
	phy_write(phydev, 0x1c, 0x8748);
	phy_write(phydev, 0x1c, 0xBF87);
	phy_write(phydev, 0x1c, 0x5002);
	phy_write(phydev, 0x1c, 0x3C5F);
	phy_write(phydev, 0x1c, 0xAC28);
	phy_write(phydev, 0x1c, 0x03AF);
	phy_write(phydev, 0x1c, 0x8748);
	phy_write(phydev, 0x1c, 0xBF87);
	phy_write(phydev, 0x1c, 0x5302);
	phy_write(phydev, 0x1c, 0x43BB);
	phy_write(phydev, 0x1c, 0x0243);
	phy_write(phydev, 0x1c, 0xC3EE);
	phy_write(phydev, 0x1c, 0x876B);
	phy_write(phydev, 0x1c, 0x0102);
	phy_write(phydev, 0x1c, 0x42E7);
	phy_write(phydev, 0x1c, 0xEF47);
	phy_write(phydev, 0x1c, 0xE487);
	phy_write(phydev, 0x1c, 0x6CE5);
	phy_write(phydev, 0x1c, 0x876D);
	phy_write(phydev, 0x1c, 0xAF87);
	phy_write(phydev, 0x1c, 0x48BF);
	phy_write(phydev, 0x1c, 0x8756);
	phy_write(phydev, 0x1c, 0x023C);
	phy_write(phydev, 0x1c, 0x5FAC);
	phy_write(phydev, 0x1c, 0x281A);
	phy_write(phydev, 0x1c, 0xE087);
	phy_write(phydev, 0x1c, 0x6CE1);
	phy_write(phydev, 0x1c, 0x876D);
	phy_write(phydev, 0x1c, 0xEF64);
	phy_write(phydev, 0x1c, 0xE087);
	phy_write(phydev, 0x1c, 0x6EE1);
	phy_write(phydev, 0x1c, 0x876F);
	phy_write(phydev, 0x1c, 0xEF74);
	phy_write(phydev, 0x1c, 0x0243);
	phy_write(phydev, 0x1c, 0x02AC);
	phy_write(phydev, 0x1c, 0x5035);
	phy_write(phydev, 0x1c, 0x00AF);
	phy_write(phydev, 0x1c, 0x8748);
	phy_write(phydev, 0x1c, 0xBF87);
	phy_write(phydev, 0x1c, 0x5902);
	phy_write(phydev, 0x1c, 0x43BB);
	phy_write(phydev, 0x1c, 0x0243);
	phy_write(phydev, 0x1c, 0xC3EE);
	phy_write(phydev, 0x1c, 0x876B);
	phy_write(phydev, 0x1c, 0x02BF);
	phy_write(phydev, 0x1c, 0x8765);
	phy_write(phydev, 0x1c, 0x0243);
	phy_write(phydev, 0x1c, 0xBBBF);
	phy_write(phydev, 0x1c, 0x875C);
	phy_write(phydev, 0x1c, 0x0243);
	phy_write(phydev, 0x1c, 0xBBBF);
	phy_write(phydev, 0x1c, 0x8762);
	phy_write(phydev, 0x1c, 0x0243);
	phy_write(phydev, 0x1c, 0xBBEE);
	phy_write(phydev, 0x1c, 0x8774);
	phy_write(phydev, 0x1c, 0x0002);
	phy_write(phydev, 0x1c, 0x42E7);
	phy_write(phydev, 0x1c, 0xEF47);
	phy_write(phydev, 0x1c, 0xE487);
	phy_write(phydev, 0x1c, 0x70E5);
	phy_write(phydev, 0x1c, 0x8771);
	phy_write(phydev, 0x1c, 0xAF87);
	phy_write(phydev, 0x1c, 0x48EE);
	phy_write(phydev, 0x1c, 0x876B);
	phy_write(phydev, 0x1c, 0x00EE);
	phy_write(phydev, 0x1c, 0x8775);
	phy_write(phydev, 0x1c, 0x01AF);
	phy_write(phydev, 0x1c, 0x8748);
	phy_write(phydev, 0x1c, 0xE087);
	phy_write(phydev, 0x1c, 0x70E1);
	phy_write(phydev, 0x1c, 0x8771);
	phy_write(phydev, 0x1c, 0xEF64);
	phy_write(phydev, 0x1c, 0xE087);
	phy_write(phydev, 0x1c, 0x72E1);
	phy_write(phydev, 0x1c, 0x8773);
	phy_write(phydev, 0x1c, 0xEF74);
	phy_write(phydev, 0x1c, 0x0243);
	phy_write(phydev, 0x1c, 0x02AD);
	phy_write(phydev, 0x1c, 0x504E);
	phy_write(phydev, 0x1c, 0xBF87);
	phy_write(phydev, 0x1c, 0x5C02);
	phy_write(phydev, 0x1c, 0x3C5F);
	phy_write(phydev, 0x1c, 0x3C00);
	phy_write(phydev, 0x1c, 0x009F);
	phy_write(phydev, 0x1c, 0x0DBF);
	phy_write(phydev, 0x1c, 0x875F);
	phy_write(phydev, 0x1c, 0x023C);
	phy_write(phydev, 0x1c, 0x5F3C);
	phy_write(phydev, 0x1c, 0x0000);
	phy_write(phydev, 0x1c, 0x9F02);
	phy_write(phydev, 0x1c, 0xAE27);
	phy_write(phydev, 0x1c, 0xBF87);
	phy_write(phydev, 0x1c, 0x6502);
	phy_write(phydev, 0x1c, 0x43BB);
	phy_write(phydev, 0x1c, 0xBF87);
	phy_write(phydev, 0x1c, 0x5C02);
	phy_write(phydev, 0x1c, 0x43BB);
	phy_write(phydev, 0x1c, 0xBF87);
	phy_write(phydev, 0x1c, 0x6202);
	phy_write(phydev, 0x1c, 0x43BB);
	phy_write(phydev, 0x1c, 0xE187);
	phy_write(phydev, 0x1c, 0x7411);
	phy_write(phydev, 0x1c, 0xE587);
	phy_write(phydev, 0x1c, 0x7439);
	phy_write(phydev, 0x1c, 0x03AA);
	phy_write(phydev, 0x1c, 0x19EE);
	phy_write(phydev, 0x1c, 0x876B);
	phy_write(phydev, 0x1c, 0x00EE);
	phy_write(phydev, 0x1c, 0x8776);
	phy_write(phydev, 0x1c, 0x01AE);
	phy_write(phydev, 0x1c, 0x0F02);
	phy_write(phydev, 0x1c, 0x42E7);
	phy_write(phydev, 0x1c, 0xEF47);
	phy_write(phydev, 0x1c, 0xE487);
	phy_write(phydev, 0x1c, 0x70E5);
	phy_write(phydev, 0x1c, 0x8771);
	phy_write(phydev, 0x1c, 0xEE87);
	phy_write(phydev, 0x1c, 0x7400);
	phy_write(phydev, 0x1c, 0xFFFE);
	phy_write(phydev, 0x1c, 0xFDEF);
	phy_write(phydev, 0x1c, 0x95FD);
	phy_write(phydev, 0x1c, 0xFC04);
	phy_write(phydev, 0x1c, 0xEECF);
	phy_write(phydev, 0x1c, 0x0411);
	phy_write(phydev, 0x1c, 0xCC00);
	phy_write(phydev, 0x1c, 0x00CC);
	phy_write(phydev, 0x1c, 0x7A00);
	phy_write(phydev, 0x1c, 0xD04A);
	phy_write(phydev, 0x1c, 0xF0CC);
	phy_write(phydev, 0x1c, 0x40F8);
	phy_write(phydev, 0x1c, 0xCC42);
	phy_write(phydev, 0x1c, 0xF0CC);
	phy_write(phydev, 0x1c, 0x4420);
	phy_write(phydev, 0x1c, 0xCC70);
	phy_write(phydev, 0x1c, 0x11CE);
	phy_write(phydev, 0x1c, 0x0000);
	phy_write(phydev, 0x1c, 0x0000);
	phy_write(phydev, 0x1c, 0x003C);
	phy_write(phydev, 0x1c, 0x0000);
	phy_write(phydev, 0x1c, 0x003C);
	phy_write(phydev, 0x1c, 0x0000);
	phy_write(phydev, 0x1c, 0x0000);
	phy_write(phydev, 0x1c, 0x0000);
	phy_write(phydev, 0x1c, 0x0000);

	rtl9010ax_write_spr(phydev, 0xB818, 0x0967);
	rtl9010ax_write_spr(phydev, 0xB81A, 0x0000);
	rtl9010ax_write_spr(phydev, 0xB81C, 0x0000);
	rtl9010ax_write_spr(phydev, 0xB81E, 0x0000);
	rtl9010ax_write_spr(phydev, 0xB832, 0x0001);
	rtl9010ax_write_spr(phydev, 0xB82E, 0x0000);
	rtl9010ax_write_spr(phydev, 0x8020, 0x0000);
	rtl9010ax_write_spr(phydev, 0xB820, 0x0000);
	timer = 3;
	for (;;) {
		ret = rtl9010ax_read_spr(phydev, 0xB800);
		if (ret < 0)
			return ret;
		if (!(ret & 0x0040))
			break;
		if (--timer == 0)
			return -ETIMEDOUT;
		mdelay(1);
	}

	rtl9010ax_soft_reset(phydev);

	return 0;
}

static int rtl9010ax_initial(struct phy_device *phydev)
{
	int ret;

	ret = rtl9010ax_initial_config(phydev);
	if (ret != 0) {
		phydev_warn(phydev, "rtl9010ax init config fail\n");
		return ret;
	}

	if (phydev->interface == PHY_INTERFACE_MODE_SGMII) {
		phydev_info(phydev, "rtl9010ax sgmii init\n");
		ret = rtl9010ax_initial_sgmii(phydev);
	}

	return ret;
}

static int rtl9010ax_rgmii_driving_strength(struct phy_device *phydev, u16 RGMII_Voltage)
{

	phydev_info(phydev, "config rtl9010ax driving strength to %u\n", RGMII_Voltage);

	switch (RGMII_Voltage){
	case Weak_RGMII_3V3:
		rtl9010ax_write_spr(phydev, 0xd414, 0x0605);
		rtl9010ax_write_spr(phydev, 0xd416, 0x0505);
		rtl9010ax_write_spr(phydev, 0xd418, 0x0600);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x0500);
		rtl9010ax_write_spr(phydev, 0xd42e, 0xa3a3);
		break;

	case Typical_RGMII_3V3:
		rtl9010ax_write_spr(phydev, 0xd414, 0x1615);
		rtl9010ax_write_spr(phydev, 0xd416, 0x1515);
		rtl9010ax_write_spr(phydev, 0xd418, 0x1600);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x1500);
		rtl9010ax_write_spr(phydev, 0xd42e, 0x80a4);
		break;

	case Weak_RGMII_2V5:
		rtl9010ax_write_spr(phydev, 0xd414, 0x0201);
		rtl9010ax_write_spr(phydev, 0xd416, 0x0101);
		rtl9010ax_write_spr(phydev, 0xd418, 0x0200);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x0100);
		rtl9010ax_write_spr(phydev, 0xd42e, 0x2525);
		break;

	case Typical_RGMII_2V5:
		rtl9010ax_write_spr(phydev, 0xd414, 0x0201);
		rtl9010ax_write_spr(phydev, 0xd416, 0x0101);
		rtl9010ax_write_spr(phydev, 0xd418, 0x0200);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x0100);
		rtl9010ax_write_spr(phydev, 0xd42e, 0xffff);
		break;

	case Weak_RGMII_1V8:
		rtl9010ax_write_spr(phydev, 0xd414, 0x0201);
		rtl9010ax_write_spr(phydev, 0xd416, 0x0101);
		rtl9010ax_write_spr(phydev, 0xd418, 0x0200);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x0100);
		rtl9010ax_write_spr(phydev, 0xd42e, 0xc8c8);
		break;

	case Typical_RGMII_1V8:
		rtl9010ax_write_spr(phydev, 0xd414, 0x1211);
		rtl9010ax_write_spr(phydev, 0xd416, 0x1111);
		rtl9010ax_write_spr(phydev, 0xd418, 0x1200);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x1100);
		rtl9010ax_write_spr(phydev, 0xd42e, 0x7e7e);
		break;

	default:
		break;
	}
	return 0;
}

/**
 * rtl9010ax_1000m_txc_delay - config 1000M TXC delay
 * on a given PHY.
 * @phydev: The phy_device struct
 * @delay:  The delay time to config
 * @speed:  The delay adjust speed
 *
 * Returns negative errno, 1 in case of change
 */
#define rtl9010ax_delay_effect(phydev)   rtl9010ax_soft_reset(phydev)
static int rtl9010ax_1000m_txc_delay(struct phy_device *phydev,
				u16 delay, u16 speed)
{
	int ret;
	u16 rgtr2;
	u16 rgtr3;

	if (delay > 0x0003 || speed > 0x0003) {
        return -EINVAL;
	}

	ret = rtl9010ax_read_spr(phydev, 0xd084);
	rgtr2 = (u16)ret;
	if (ret < 0)
		return ret;

	ret = rtl9010ax_read_spr(phydev, 0xd082);
	rgtr3 = (u16)ret;
	if (ret < 0)
		return ret;

	/* use the following step to change the
	 * level of 1000M_RGMII_TX_Timing
	 */

	/* step1: Set 1000M_TXC_nodelay_en(bit7) to 0*/
	rgtr2 &= ~0x0080;
	rtl9010ax_write_spr(phydev, 0xd084, rgtr2);
	rtl9010ax_delay_effect(phydev);

	/* step2: Set 1000M_RGMII_TXC_timing_delay_adjust_speed (bit[1:0])
	 * to 2'b11
	 */
	rgtr3 |= 0x0003;
	rtl9010ax_write_spr(phydev, 0xd082, rgtr3);

	/* step3: Change 1000M_RGMII_TXC_Timing(bit[15:14])
	 * to desired level
	 */
	rgtr2 &= ~0xc000;
	rgtr2 |= ((delay << 14) & 0xc000);
	rtl9010ax_write_spr(phydev, 0xd084, rgtr2);
	rtl9010ax_delay_effect(phydev);

	/* step4: Set 1000M_RGMII_TXC_timing_delay_en(bit[2:0]) to 3'b111*/
	rgtr2 |= 0x0007;
	rtl9010ax_write_spr(phydev, 0xd084, rgtr2);
	rtl9010ax_delay_effect(phydev);

	/* step5: if the user want to change the level, set
	 * 1000M_RGMII_TXC_timing_delay_en(bit[2:0]) to 3'b000
	 */
	if (speed != 0x0003) {
		rgtr2 &= ~0x0007;
		rtl9010ax_write_spr(phydev, 0xd084, rgtr2);
		rtl9010ax_delay_effect(phydev);

		/* step6: change 1000M_RGMII_TXC_timing_delay_adjust_speed(bit[1:0])
		 * to desired level
		 */
		rgtr3 &= ~0x0003;
		rgtr3 |= (speed & 0x0003);
		rtl9010ax_write_spr(phydev, 0xd082, rgtr3);
		rtl9010ax_delay_effect(phydev);

		/* step7: set 1000M_RGMII_TXC_timing_delay_en(bit[2:0])
		 * to 3'b111 again
		 */
		rgtr2 |= 0x0007;
		rtl9010ax_write_spr(phydev, 0xd084, rgtr2);
		rtl9010ax_delay_effect(phydev);
	}

	if (delay == 0x0003) { //no delay
		rgtr2 |= 0x0080;
		rgtr2 &= ~0x0007;
		rtl9010ax_write_spr(phydev, 0xd084, rgtr2);
		rtl9010ax_delay_effect(phydev);
	} else {
		/* TODO: Set 1000M_TXC_nodelay_en(bit7) to 1
		 * rgtr2 |= 0x0080;
		 * rtl9010ax_write_spr(phydev, 0xd084, rgtr2);
		 */
	}

	return 0;
}

static int rtl9010ax_1000m_rxc_delay(struct phy_device *phydev, u16 delay)
{
	int ret;
	u16 rgtr1;

	ret = rtl9010ax_read_spr(phydev, 0xd04A);
	rgtr1 = (u16)ret;
	if (ret < 0)
		return ret;

	if (delay)
		rgtr1 |= 0x0004;
	else
		rgtr1 &= ~0x0004;

	ret = rtl9010ax_write_spr(phydev, 0xd04A, rgtr1);
	rtl9010ax_delay_effect(phydev);

	return ret;
}

static void rtl9010ax_config_delay(struct phy_device *phydev)
{
	int ret;
	u16 val_txdly, val_rxdly;
	/**
	 * val_txdly:
	 * 0x00:  0~2ns
	 * 0x01:  1~4ns
	 * 0x02:  1.5~6ns
	 * 0x03:  no delay
	 *
	 * val_rxdly:
	 * 0x00:  no delay
	 * 0x01:  2ns delay
	 */
	switch (phydev->interface) {
	case PHY_INTERFACE_MODE_RGMII:
		// val_txdly = 0x03; no delay
		// val_rxdly = 0x00; no delay
		val_txdly = 0x02; // delay 1.5~6ns
		val_rxdly = 0x01; // delay 2ns
		break;

	case PHY_INTERFACE_MODE_RGMII_RXID:
		val_txdly = 0x03; // no delay
		val_rxdly = 0x01; // delay 2ns
		break;

	case PHY_INTERFACE_MODE_RGMII_TXID:
		val_txdly = 0x02; // delay 1.5~6ns
		val_rxdly = 0x00; // no delay
		break;

	case PHY_INTERFACE_MODE_RGMII_ID:
		val_txdly = 0x02; // delay 1.5~6ns
		val_rxdly = 0x01; // delay 2ns
		break;

	default: /* the rest of the modes imply leaving delay as is. */
		return;
	}

	ret = rtl9010ax_1000m_txc_delay(phydev, val_txdly, 0x03);
	if (ret < 0)
		phydev_err(phydev, "Failed to update the TX delay register\n");

	ret = rtl9010ax_1000m_rxc_delay(phydev, val_rxdly);
	if (ret < 0)
		phydev_err(phydev, "Failed to update the RX delay register\n");
}

static int rtl9010ax_probe(struct phy_device *phydev)
{
	struct device *dev = &phydev->mdio.dev;
	struct rtl9010ax_priv *priv;

	priv = devm_kzalloc(dev, sizeof(*priv), GFP_KERNEL);
	if (!priv)
		return -ENOMEM;

	phydev->priv = priv;

	priv->master_slave = PHY_MASTER_SLAVE_CFG;

	return 0;
}

static int rtl9010ax_config_init(struct phy_device *phydev)
{
	struct rtl9010ax_priv *priv = phydev->priv;

	phydev_info(phydev, "rtl9010ax_config_init\n");

	phydev->autoneg = AUTONEG_DISABLE;
	phydev->speed = SPEED_1000;
	phydev->duplex = DUPLEX_FULL;

	// load Errata
	rtl9010ax_initial(phydev);

	// power select
	phy_modify_paged_changed(phydev, 0x0a4c, 0x12, 0x3800, 0x10FF);

	/*config txc and rxc dealy*/
	rtl9010ax_config_delay(phydev);

	/* adjust the driving strength for rgmii*/
	if (phydev->interface == PHY_INTERFACE_MODE_RGMII ||
			phydev->interface == PHY_INTERFACE_MODE_RGMII_ID ||
			phydev->interface == PHY_INTERFACE_MODE_RGMII_RXID ||
			phydev->interface == PHY_INTERFACE_MODE_RGMII_TXID )
		rtl9010ax_rgmii_driving_strength(phydev, Weak_RGMII_3V3);

	/* adjust tx amplitude for sgmii
	 * 0x0048 --- 700mv, 0x005a --- 580mv, 0x006c --- 450mv
	 */
	if (phydev->interface == PHY_INTERFACE_MODE_SGMII)
		phy_write_paged(phydev, 0x0cd1, 0x16, 0x0048);

	/* config work mode*/
	if (priv->master_slave == MASTER_SLAVE_CFG_MASTER_FORCE)
		phy_write(phydev, MII_CTRL1000, CTL1000_AS_MASTER);
	else
		phy_write(phydev, MII_CTRL1000, 0x0000); //slave

	return rtl9010ax_soft_reset(phydev);
}

static int rtl9010ax_config_aneg(struct phy_device *phydev)
{
#if 0
	int ret;
	u16 ctl = 0;

	switch (phydev->master_slave_set) {
	case MASTER_SLAVE_CFG_MASTER_FORCE:
		ctl |= CTL1000_AS_MASTER;
		break;
	case MASTER_SLAVE_CFG_SLAVE_FORCE:
		break;
	case MASTER_SLAVE_CFG_UNKNOWN:
	case MASTER_SLAVE_CFG_UNSUPPORTED:
		return 0;
	default:
		phydev_warn(phydev, "Unsupported Master/Slave mode\n");
		return -EOPNOTSUPP;
	}

	ret = phy_modify_changed(phydev, MII_CTRL1000, CTL1000_AS_MASTER, ctl);
	if (ret == 1) {
		phydev->speed = SPEED_1000;
		phydev->duplex = DUPLEX_FULL;
		ret = rtl9010ax_soft_reset(phydev);
	}

	return ret;
#else
	return 0;
#endif
}

static int rtl9010ax_read_status(struct phy_device *phydev)
{
	int status;
	/**
	 * PHYSR2
	 * 15:12   Reserved
	 * 11      Role:   1 -- master mode, 0 -- slave mode
	 * 10:6    Reserved
	 * 5:4     Speed:  01 -- 100M, 10 -- 1000M
	 * 3       duplex: 1  -- Full,  0 -- Half
	 * 2       Real-time Link Status: 1 -- Link, 0 -- Not Link
	 * 1:0     Reserved
	 */

	/* read the PHYSR2*/
	status = phy_read(phydev, 0x1a);
	if (status < 0)
		return status;

	// Update the link
	if (status & 0x0004)
		phydev->link = 1;
	else
		phydev->link = 0;

	if (status & 0x0008)
		phydev->duplex = DUPLEX_FULL;
	else
		phydev->duplex = DUPLEX_HALF;

	if ((status & 0x0030) == 0x0020)
		phydev->speed = SPEED_1000;
	else if ((status & 0x0030) == 0x0010)
		phydev->speed = SPEED_100;
	else
		phydev->speed = SPEED_UNKNOWN;

	if (phydev->autoneg == AUTONEG_ENABLE) {
		if (status & 0x0800)
			phydev->master_slave_state = MASTER_SLAVE_STATE_MASTER;
		else
			phydev->master_slave_state = MASTER_SLAVE_STATE_SLAVE;
	}

	return  0;
}

static struct phy_driver realtek_drvs[] = {
	{
		PHY_ID_MATCH_EXACT(0x00008201),
		.name           = "RTL8201CP Ethernet",
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc816),
		.name		= "RTL8201F Fast Ethernet",
		.config_intr	= &rtl8201_config_intr,
		.handle_interrupt = rtl8201_handle_interrupt,
		.suspend	= genphy_suspend,
		.resume		= genphy_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		PHY_ID_MATCH_MODEL(0x001cc880),
		.name		= "RTL8208 Fast Ethernet",
		.read_mmd	= genphy_read_mmd_unsupported,
		.write_mmd	= genphy_write_mmd_unsupported,
		.suspend	= genphy_suspend,
		.resume		= genphy_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc910),
		.name		= "RTL8211 Gigabit Ethernet",
		.config_aneg	= rtl8211_config_aneg,
		.read_mmd	= &genphy_read_mmd_unsupported,
		.write_mmd	= &genphy_write_mmd_unsupported,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc912),
		.name		= "RTL8211B Gigabit Ethernet",
		.config_intr	= &rtl8211b_config_intr,
		.handle_interrupt = rtl821x_handle_interrupt,
		.read_mmd	= &genphy_read_mmd_unsupported,
		.write_mmd	= &genphy_write_mmd_unsupported,
		.suspend	= rtl8211b_suspend,
		.resume		= rtl8211b_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc913),
		.name		= "RTL8211C Gigabit Ethernet",
		.config_init	= rtl8211c_config_init,
		.read_mmd	= &genphy_read_mmd_unsupported,
		.write_mmd	= &genphy_write_mmd_unsupported,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc914),
		.name		= "RTL8211DN Gigabit Ethernet",
		.config_intr	= rtl8211e_config_intr,
		.handle_interrupt = rtl821x_handle_interrupt,
		.suspend	= genphy_suspend,
		.resume		= genphy_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc915),
		.name		= "RTL8211E Gigabit Ethernet",
		.config_init	= &rtl8211e_config_init,
		.config_intr	= &rtl8211e_config_intr,
		.handle_interrupt = rtl821x_handle_interrupt,
		.suspend	= genphy_suspend,
		.resume		= genphy_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc916),
		.name		= "RTL8211F Gigabit Ethernet",
		.probe		= rtl821x_probe,
		.config_init	= &rtl8211f_config_init,
		.read_status	= rtlgen_read_status,
		.config_intr	= &rtl8211f_config_intr,
		.handle_interrupt = rtl8211f_handle_interrupt,
		.suspend	= genphy_suspend,
		.resume		= rtl821x_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		.name		= "Generic FE-GE Realtek PHY",
		.match_phy_device = rtlgen_match_phy_device,
		.read_status	= rtlgen_read_status,
		.suspend	= genphy_suspend,
		.resume		= rtlgen_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
		.read_mmd	= rtlgen_read_mmd,
		.write_mmd	= rtlgen_write_mmd,
	}, {
		.name		= "RTL8226 2.5Gbps PHY",
		.match_phy_device = rtl8226_match_phy_device,
		.get_features	= rtl822x_get_features,
		.config_aneg	= rtl822x_config_aneg,
		.read_status	= rtl822x_read_status,
		.suspend	= genphy_suspend,
		.resume		= rtlgen_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
		.read_mmd	= rtl822x_read_mmd,
		.write_mmd	= rtl822x_write_mmd,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc840),
		.name		= "RTL8226B_RTL8221B 2.5Gbps PHY",
		.get_features	= rtl822x_get_features,
		.config_aneg	= rtl822x_config_aneg,
		.read_status	= rtl822x_read_status,
		.suspend	= genphy_suspend,
		.resume		= rtlgen_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
		.read_mmd	= rtl822x_read_mmd,
		.write_mmd	= rtl822x_write_mmd,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc838),
		.name           = "RTL8226-CG 2.5Gbps PHY",
		.get_features   = rtl822x_get_features,
		.config_aneg    = rtl822x_config_aneg,
		.read_status    = rtl822x_read_status,
		.suspend        = genphy_suspend,
		.resume         = rtlgen_resume,
		.read_page      = rtl821x_read_page,
		.write_page     = rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc848),
		.name           = "RTL8226B-CG_RTL8221B-CG 2.5Gbps PHY",
		.get_features   = rtl822x_get_features,
		.config_aneg    = rtl822x_config_aneg,
		.read_status    = rtl822x_read_status,
		.suspend        = genphy_suspend,
		.resume         = rtlgen_resume,
		.read_page      = rtl821x_read_page,
		.write_page     = rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc849),
		.name           = "RTL8221B-VB-CG 2.5Gbps PHY",
		.get_features   = rtl822x_get_features,
		.config_aneg    = rtl822x_config_aneg,
		.read_status    = rtl822x_read_status,
		.suspend        = genphy_suspend,
		.resume         = rtlgen_resume,
		.read_page      = rtl821x_read_page,
		.write_page     = rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc84a),
		.name           = "RTL8221B-VM-CG 2.5Gbps PHY",
		.get_features   = rtl822x_get_features,
		.config_aneg    = rtl822x_config_aneg,
		.read_status    = rtl822x_read_status,
		.suspend        = genphy_suspend,
		.resume         = rtlgen_resume,
		.read_page      = rtl821x_read_page,
		.write_page     = rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x001cc961),
		.name		= "RTL8366RB Gigabit Ethernet",
		.config_init	= &rtl8366rb_config_init,
		/* These interrupts are handled by the irq controller
		 * embedded inside the RTL8366RB, they get unmasked when the
		 * irq is requested and ACKed by reading the status register,
		 * which is done by the irqchip code.
		 */
		.config_intr	= genphy_no_config_intr,
		.handle_interrupt = genphy_handle_interrupt_no_ack,
		.suspend	= genphy_suspend,
		.resume		= genphy_resume,
	}, {
		PHY_ID_MATCH_EXACT(0x001ccb00),
		.name		= "RTL9000AA_RTL9000AN Ethernet",
		.features       = PHY_BASIC_T1_FEATURES,
		.config_init	= rtl9000a_config_init,
		.config_aneg	= rtl9000a_config_aneg,
		.read_status	= rtl9000a_read_status,
		.config_intr	= rtl9000a_config_intr,
		.handle_interrupt = rtl9000a_handle_interrupt,
		.suspend	= genphy_suspend,
		.resume		= genphy_resume,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}, {
		PHY_ID_MATCH_EXACT(0x1ccb30),
		.name		= "RTL9010ARG Gigabit Ethernet",
		.features = PHY_GBIT_FEATURES,
		.probe		= rtl9010ax_probe,
		.config_init	= rtl9010ax_config_init,
		.config_aneg	= rtl9010ax_config_aneg,
		.read_status	= rtl9010ax_read_status,
		.read_page	= rtl821x_read_page,
		.write_page	= rtl821x_write_page,
	}
};

module_phy_driver(realtek_drvs);

static const struct mdio_device_id __maybe_unused realtek_tbl[] = {
	{ PHY_ID_MATCH_VENDOR(0x001cc800) },
	{ }
};

MODULE_DEVICE_TABLE(mdio, realtek_tbl);
