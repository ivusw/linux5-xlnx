
#ifndef __PHY_CUSTOMER_H
#define __PHY_CUSTOMER_H

/* config as
 * MASTER_SLAVE_CFG_MASTER_FORCE 
 * MASTER_SLAVE_CFG_SLAVE_FORCE
 * default:  MASTER_SLAVE_CFG_SLAVE_FORCE
 */
#define PHY_MASTER_SLAVE_CFG  MASTER_SLAVE_CFG_SLAVE_FORCE

#endif /* __PHY_CUSTOMER_H */